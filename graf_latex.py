#encoding: utf-8
#https://jdhao.github.io/2018/01/18/mpl-plotting-notes-201801/#default-math-font-is-ugly
import numpy as np #biblioteca matemática
import matplotlib
from matplotlib import pyplot as plt #biblioteca de plots
matplotlib.rcParams['mathtext.fontset'] = 'cm'


m_dados = np.loadtxt('dados.dat')
x = m_dados[:,0] #[row[0] for row in m_dados]
y = m_dados[:,1] #[row[1] for row in m_dados]


plt.ion()
plt.clf()
plt.plot(x, y, 'k.-', label=r'$\sin(x)$')
plt.xlabel('x')
plt.ylabel('y')
plt.title(r'$\epsilon, \varepsilon, \xi, \rho$')
plt.title(r'$\pi \approx 3$', loc='right')



plt.savefig('fig1_comp_modern_roman.pdf')
