#encoding: utf-8
import numpy as np

x = np.linspace(0, 5, 100);
y = np.sin(x);
y2 = np.cos(x);

mat = [x,y,y2]
np.savetxt('dados.dat', np.transpose(mat), fmt='%f')
