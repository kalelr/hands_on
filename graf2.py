#encoding: utf-8

from matplotlib import pyplot as plt #biblioteca de plots
import numpy as np #biblioteca matemática
pi = np.pi

#=====funçao para converter valores em cm para valores em inches
def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)

m_dados = np.loadtxt('dados.dat')
x = m_dados[:,0] #[row[0] for row in m_dados]
y = m_dados[:,1] #[row[1] for row in m_dados]
y2 = m_dados[:,2] #[row[2] for row in m_dados]

# x = np.linspace(0, 5, 100);
# y = 100*np.sin(x);
# y2 = np.cos(x);

plt.ion()

fig, ax1 = plt.subplots() #gera o handler ax1

ax1.plot(x, 100*y, 'k.-')
ax1.set_ylabel(r'$100 \sin(x)$')
ax1.set_ylim(-100, 100)

ax2 = ax1.twinx()
ax2.plot(x, y2, 'bo-', markersize=4, linewidth=3)
ax2.set_ylabel('cos(x)', color='b')
ax2.tick_params(axis='y', labelcolor='blue')
ax2.set_ylim(-1.05, 1.05)

plt.title('Título no centro', loc='center', fontsize=15)
plt.title('Título na esquerda', loc='left')
plt.title(r'$\pi \approx 3$', loc='right')
plt.xlim(left=0, right=5.05)
plt.xticks([0, pi/2, pi, 3*pi/2], ['0', r'$\frac{\pi}{2}$', r'$\pi$', r'$\frac{3\pi}{2}$'], fontsize=10)
plt.yticks(np.linspace(-1, 1, 5))
plt.grid(axis='y', linestyle='--')


fig.tight_layout() #sem isso, a 2a label fica quase fora da fig

fig = plt.gcf();
# (height, width) = cm2inch(10,20)
width = 20; height = 10;
fig.set_size_inches(width/2.54,height/2.54) #2.54 cm = 1 inches


# plt.savefig('fig2.png')
plt.savefig('fig2.pdf')



