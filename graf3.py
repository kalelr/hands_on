#encoding: utf-8
from matplotlib import pyplot as plt #biblioteca de plots
import numpy as np #biblioteca matemática
pi = np.pi


#=====funçao para converter valores em cm para valores em inches
def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)


# x = np.linspace(0, 5, 100);
# y = 100*np.sin(x);
# y2 = np.cos(x);
m_dados = np.loadtxt('dados.dat')
x = m_dados[:,0] #[row[0] for row in m_dados]
y = m_dados[:,1] #[row[1] for row in m_dados]
y2 = m_dados[:,2] #[row[2] for row in m_dados]



numL = 2;
numC = 1;

plt.ion()
fig, ax = plt.subplots(numL, numC, sharex='col') #gera o handler ax1


#=====100 * sin(x)
ax[0].plot(x, 100*y, 'k.-', label=r'$100 \sin(x)$')
ax[0].legend(loc='best')
ax[0].set_ylabel('y')
ax[0].set_title('Título no centro', loc='center', fontsize=15)
ax[0].set_title('Título na esquerda', loc='left')
ax[0].set_title(r'$\pi \approx 3$', loc='right')
ax[0].set_ylim(-100, 100)
ax[0].set_yticks(np.linspace(-100, 100, 5))
ax[0].grid(axis='y', linestyle='--')

#====cos(x)===
ax[1].plot(x, y2, 'b.-', label=r'$\cos(x)$')
ax[1].legend(loc='best')
ax[1].set_ylabel('y')
ax[1].set_title('Centro, baixo', loc='center', fontsize=15)
ax[1].set_title('Esquerda, baixo', loc='left')
ax[1].set_title(r'$\pi \approx 3$', loc='right')
ax[1].set_ylim(-1, 1)
ax[1].set_yticks(np.linspace(-1, 1, 5))
ax[1].grid(axis='y', linestyle='--')

#==eixo x
ax[1].set_xlabel('x')
ax[1].set_xlim(0, 5.05)
ax[1].set_xticks([0, pi/2, pi, 3*pi/2])
ax[1].set_xticklabels(['0', r'$\frac{\pi}{2}$', r'$\pi$', r'$\frac{3\pi}{2}$'], fontsize=10)


fig.suptitle('Título geral')
fig = plt.gcf();
(height, width) = cm2inch(10,20)
fig.set_size_inches(width,height)


fig.tight_layout(rect=[0, 0.03, 1, 0.95], pad=1.08) #[left, bottom, right, top]
# fig.subplots_adjust(left=0.125, right=0.9, bottom=0.1, top=0.9, wspace=0.2, hspace=0.2) #valores padrao



# fig.savefig('fig3.png')
fig.savefig('fig3.pdf')



