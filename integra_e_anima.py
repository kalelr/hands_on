import numpy as np 
from scipy.integrate import odeint
import matplotlib.pyplot as plt 
import matplotlib.animation as animation


def model(y, t, w0, b):
	""" OHS amortecido """
	
	x = y[0];
	v = y[1];
	dxdt = v
	dvdt = -w0**2 *x - b*v;
	
	return [dxdt, dvdt]



x0, v0 = 0.0, 1.0
y0 = [x0,v0]

w0, b = 1.0, 0.0

t = np.linspace(0, 20, 100)

y = odeint(model, y0, t, args=(w0,b))

fig = plt.figure()
ax1 = fig.add_subplot(2,1,1)
ax2 = fig.add_subplot(2,1,2)

lines = []

for i in range(len(t)):
	head = i - 1;
	head_slice = (t > t[i] - 1.0) & (t < t[i])
	
	# line1, = ax1.plot( t[:i], y[:i,0], color='black')
	# line1a, = ax1.plot(t[head], y[head,0], color='red', marker='o', markersize=20-i*0.3)
	line1a, = ax1.plot(t[head], y[head,0], color=[0, 0, i/len(t)] , marker='o', markersize=20-i*0.3)
	line1b, = ax1.plot(t[head_slice], y[head_slice, 0], color='red')
	# line1b, = ax1.plot(t[head_slice], y[head_slice, 0], color=[0,0, i/len(t)])
	
	line2, = ax2.plot(t[:i], y[:i,1], color='blue')
	lines.append([line1a, line1b, line2])



ani = animation.ArtistAnimation(fig, lines, interval=100) #interval: freq de amostragem

ani.save('anima.mp4', writer='ffmpeg')

# plt.show()

