#encoding: utf-8

import numpy as np #biblioteca matemática
from matplotlib import pyplot as plt #biblioteca de plots
pi = np.pi


#=====funçao para converter valores em cm para valores em inches
def cm2inch(*tupl):
    inch = 2.54
    if isinstance(tupl[0], tuple):
        return tuple(i/inch for i in tupl[0])
    else:
        return tuple(i/inch for i in tupl)



x = np.linspace(0, 5, 100);
y = np.sin(x);
y2 = np.cos(x);


plt.ion()
plt.clf()
plt.plot(x, y, 'k.-', label=r'$\sin(x)$')
plt.plot(x, y2, 'bo-', markersize=4, linewidth=3, label='cos(x)')
plt.legend(loc='best')
plt.xlabel('x')
plt.ylabel('y')
plt.title(u'Título no centro', loc='center', fontsize=15)
plt.title(u'Título na esquerda', loc='left')
plt.title(r'$\pi \approx 3$', loc='right')


plt.xlim(left=0, right=5.05)
plt.ylim(-1.05, 1.05)

plt.xticks([0, pi/2, pi, 3*pi/2], ['0', r'$\frac{\pi}{2}$', r'$\pi$', r'$\frac{3\pi}{2}$'], fontsize=10)
plt.yticks(np.linspace(-1, 1, 5))
plt.grid(axis='y', linestyle='--')

fig = plt.gcf();
(height, width) = cm2inch(10,20)
fig.set_size_inches(width,height)


plt.savefig('fig1.pdf')

#plt.savefig('fig1.eps', format='eps', dpi=1000)


#-*- coding: UTF-8 -*-
#from __future__ import unicode_literals #ao invés disso, pode colocar u antes das aspas ('') nas strings 
# from __future__ import unicode_literals
#-*- cofing: UTF-8 -*-
